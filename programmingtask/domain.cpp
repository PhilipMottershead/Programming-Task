/**
 *This class is used to store orders that have the same domain
 *
 *@author Philip Mottershead
 *@date 14/05/2018
*/

#include "domain.h"
#include <utility>
#include <algorithm>
#include <iostream>

/**
 * Function that compares if the order are in alphabetical order by email_address
 * @param order1 First order to compare
 * @param order2 secound order to compare
 * @return True if the orders are in alphabetical order
 */
bool compare_alphabetical(order &order1, order &order2){
    return order1.get_email_address()< order2.get_email_address();}

/**
 * Add a order to the domain
 * @param order
 */
void domain::add_to_domain(const order order){
    orders.push_back(order);
}

/**
 * Gets the name of the domain
 * @return
 */
const string &domain::get_name() const {
    return name;
}

/**
 * Constructor for the domain
 * @param name name of the domain, the part after the @ in the email address
 */
domain::domain(string name) : name(std::move(name)) {}

/**
 * Sorts the orders into alphabetical order
 */
void domain::sort_domain() {
    sort(orders.begin(), orders.end(), compare_alphabetical);
}

/**
 * Returns a formatted string of a domain
 * @return A formatted string containing all data about a domain including the order contained within
 */
string domain::print_domain() {
    string output_string="Orders from "+name+"\n\n";
    for(const auto &order:orders){
        output_string=output_string.append(order.print_order()+"\n");
    }
    return output_string;
}