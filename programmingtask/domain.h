/**
 *This class is used to store orders that have the same domain
 *
 *@author Philip Mottershead
 *@date 14/05/2018
*/
#ifndef IONCUBEPROGRAMMINGTASK_DOMAIN_H
#define IONCUBEPROGRAMMINGTASK_DOMAIN_H


#include <ostream>
#include <vector>
#include "order.h"

class domain {

private:

    string name;
    vector<order> orders;

public:

    const string &get_name() const;

    explicit domain(string name);

    void add_to_domain(order order);

    void sort_domain();

    string print_domain();
};

#endif //IONCUBEPROGRAMMINGTASK_DOMAIN_H
