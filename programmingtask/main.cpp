/**
 *This class is the main class it is used to run the program and deals with reading in the files and printing the report out
 *
 *@author Philip Mottershead
 *@date 14/05/2018
*/

#include <iostream>
#include <fstream>
#include <vector>
#include "order.h"
#include "main.h"

/**
 * Gives the user options of what they can do
 *
 * To load a file you first open then load it.
 */
int main() {
    vector<order> orders;
    ifstream inFile;
    string filename;
    int choice = 0;
    orderSystem orderSystem1;

    while (choice != -1) {
        choice = getChoice(choice);
        switch (choice) {
            //open file
            case 1: {
                load_file(inFile, filename);
                break;
            }
            //load file
            case 2: {
                while (inFile.good()) {
                    //checks if the first letter is a number if it load it in as data
                    int c = inFile.peek();
                    if (isdigit(c)) {

                        string id;
                        string email_address;
                        string number_of_orders;
                        string total_order_value;

                        char delimiter = ':';


                        getline(inFile, id, delimiter);
                        getline(inFile, email_address, delimiter);
                        getline(inFile, number_of_orders, delimiter);
                        getline(inFile, total_order_value, '\n');

                        //converts the id and number_of_orders to int and total_order_value to double
                        order order1(stoi(id), email_address, stoi(number_of_orders), stod(total_order_value));
                        orders.push_back(order1);

                        //else read the data in until end of line
                    } else {
                        string temp;//variable to store the not needed data
                        getline(inFile, temp, '\n');
                    }
                }
                inFile.close();

                orderSystem1.sort_into_domains(orders);
                orders.clear();
                cout<<"Loaded "<<filename+"\n";
                break;
            }
            //opens output menu
            case 3: {
                output_report(filename, orderSystem1);
                break;
            }
            //quits program
            case -1:{
                cout<<"Quiting Program";
                break;
            }
            default: {
                cout << "Invalid option\n";
                break;
            }
        }
    }
    return 0;
}

/**
 * Tries to load from file
 * @param inFile ifstream to open the file
 * @param filename filename variable
 */
void load_file(ifstream &inFile, string &filename) {
    cout << "Please enter filename\n";
    cin >> filename;

    //Tries to open the file
    inFile.open("../" + filename);
    if (!inFile) {
        cerr << "Unable to open file " << filename << "\n";
        inFile.close();
    } else {
        cout << "Opened " << filename << "\n\n";
    }
}

/**
 * Gets the users choice for the command line
 * @param choice Choice varible
 * @return The choice user selected
 */
int getChoice(int choice) {
    cout << "Enter Option:\n"
            "1 - Open file\n"
            "2 - Load file\n"
            "3 - Create report\n"
            "-1 - Quit\n";
    cin >> choice;
    return choice;
}

/**
* Gives the user options to output the report
* @param filename filename of the
* @param orderSystem1
*/
void output_report(string &filename, orderSystem &orderSystem) {
    int choice;


    cout << "How would you like to output the report?\n"
            "0 for on commandline\n"
            "1 for output to file\n";
    cin >> choice;
    switch (choice) {
        //prints onto commandline
        case 0: {
            cout << orderSystem.print_domains();
            break;
        }
        //prints to file
        case 1: {
            cout << "Enter output filename\n";
            cin >> filename;
            ofstream outfile;
            outfile.open("../" + filename);
            outfile << orderSystem.print_domains();
            outfile.close();
            break;
        }
        default: {
            cout << "Invalid option";
            break;
        }
    }
}
