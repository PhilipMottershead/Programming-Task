//
// Created by phm14 on 15/05/2018.
//

#ifndef IONCUBEPROGRAMMINGTASK_MAIN_H
#define IONCUBEPROGRAMMINGTASK_MAIN_H

#include "orderSystem.h"

int getChoice(int choice);

void load_file(ifstream &inFile, string &filename);

void output_report(string &filename, orderSystem &orderSystem);

#endif //IONCUBEPROGRAMMINGTASK_MAIN_H
