/**
 *This class is used to store order Information
 *
 *@author Philip Mottershead
 *@date 14/05/2018
*/

#include "order.h"
#include <utility>
#include <iomanip>
#include <sstream>


/**
 * Gets the email address of the order
 * @return
 */
string &order::get_email_address() {
    return email_address;
}

/**
 * Returns a formatted string so it can be printed
 * @return All the data formatted in a printable fashion
 */
string order::print_order()const {
    stringstream stream;//used so I can set the precision of total_order_value
    stream << fixed << setprecision(2) << email_address<<" "<<to_string(id)<<" "<<to_string(number_of_orders)<<" "<<total_order_value;
    string s = stream.str();
    return s;
}

/**
 * Constructor for order
 *
 * @param id Id of order
 * @param email_address Email address of order
 * @param number_of_orders Number of orders made
 * @param total_order_value Total value of orders made
 */
order::order(int id, string email_address, int number_of_orders, double total_order_value) : id(id), email_address(
        std::move(email_address)), number_of_orders(number_of_orders), total_order_value(total_order_value) {}

