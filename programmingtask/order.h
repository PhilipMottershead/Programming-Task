/**
 *This class is used to store order Information
 *
 *@author Philip Mottershead
 *@date 14/05/2018
*/

#ifndef IONCUBEPROGRAMMINGTASK_ORDERDATA_H
#define IONCUBEPROGRAMMINGTASK_ORDERDATA_H

#include <string>
#include <ostream>

using namespace std;

class order {

private:

    int id;
    string email_address;
    int number_of_orders;
    double total_order_value;

public:
    order(int id, string email_address, int number_of_orders, double total_order_value);

    string print_order()const;

    string & get_email_address();
};



#endif //IONCUBEPROGRAMMINGTASK_ORDERDATA_H
