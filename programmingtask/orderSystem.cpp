/**
 *This class is used to store and process Domains
 *
 *@author Philip Mottershead
 *@date 14/05/2018
*/

#include <vector>
#include <algorithm>
#include "order.h"

#include "orderSystem.h"

/**
 * Function that compares if the domains are in alphabetical order
 * @param domain1 first domain to check
 * @param domain2 second domain to check
 * @return True if it is in alphabetical order
 */
bool compare_alphabetical(const domain &domain1, const domain &domain2) { return domain1.get_name() < domain2.get_name();}


/**
 * Sorts the orders into domains
 * @param orders The raw orders list from reading in the data
 */
void orderSystem::sort_into_domains( vector<order> &orders) {
    for (auto &order : orders) {
        string email = order.get_email_address();
        string delimiter = "@";
        size_t pos = 0;
        string token;
        bool found = false;

        //splits the email address to get the domain
        pos = email.find(delimiter);
        email.erase(0, pos + delimiter.length());

        //checks if the domain exists
        for (auto &domain : domains) {
            if (domain.get_name() == email) {
                domain.add_to_domain(order);
                found = true;
                break;
            } else {
                found = false;
            }
        }

        //If the domain wasn't found one is created
        if (!found) {
            domain domain(email);
            domain.add_to_domain(order);
            domains.push_back(domain);
        }
    }
    //sorts all domains orders
    for (auto &domain : domains) {
        domain.sort_domain();
    }

    //sorts the domains into alphabetical order
    sort(domains.begin(), domains.end(), compare_alphabetical);
}

/**
 * Returns a formatted string of a domains
 * @return A formatted string containing all data about a domains
 */
string orderSystem::print_domains() {
    string string1;
    for (auto &domain : domains){
        string1=string1.append(domain.print_domain()+"\n");
    }
    return string1;
}

