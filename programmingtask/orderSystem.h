/**
 *This class is used to store and process Domains
 *
 *@author Philip Mottershead
 *@date 14/05/2018
*/

#ifndef IONCUBEPROGRAMMINGTASK_ORDERSYSTEM_H
#define IONCUBEPROGRAMMINGTASK_ORDERSYSTEM_H

#include <vector>
#include "domain.h"

class orderSystem{

private:
    vector<domain> domains;
public:

    void sort_into_domains(vector<order> &orders);

    string print_domains();
};


#endif //IONCUBEPROGRAMMINGTASK_ORDERSYSTEM_H
